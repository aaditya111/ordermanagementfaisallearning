import { Injectable,HttpException ,HttpStatus} from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Product } from '../entities/product.entity';
import { CreateProductDto, UpdateProductDto } from '../dtos/dto';
import {NotFoundException} from '@nestjs/common';
@Injectable()
export class ProductService {
    constructor(
        //@InjectModel(Product)
        //private productModel: typeof Product,
    ) { }
    async getData(): Promise<Product[]> {
        const products = await Product.findAll();
        return products;
    }
    async getDatabyId(id: number): Promise<Product> {        
            const dataReceived = Product.findByPk(id);
            if (dataReceived === null) {
              {
                throw new HttpException('BadRequest', HttpStatus.BAD_REQUEST);
              }
              } else {
                return dataReceived;
              }      
    }
    async addData(productDto: CreateProductDto): Promise<Product> {
        return Product.create(productDto);
      }
    async updateData(id: number, productDto: UpdateProductDto): Promise<Product> {
        const product = await Product.findByPk(id);
        return product.update(productDto);
      }
    async deleteData(id: number): Promise<string> {
        const product = await Product.findByPk(id);
        await product.destroy();
        return "Deleted Successfully";
      }
}