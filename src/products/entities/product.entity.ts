import { Table, Column, Model, AutoIncrement, PrimaryKey, CreatedAt, UpdatedAt  } from 'sequelize-typescript';

@Table
export class Product extends Model<Product> {
  @AutoIncrement
  @PrimaryKey
  @Column
  id: number;

  @Column
  name: string;

  @Column
  description: string;

  @Column
  brand: string;

  @Column
  price: number;

  @Column
  quantity: number;

  @CreatedAt
  createdAt: Date;

  @UpdatedAt
  updatedAt: Date;
}
